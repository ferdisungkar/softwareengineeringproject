package com.project.gobarber.servive;

import com.project.gobarber.model.User;
import com.project.gobarber.model.UserDetailsImpl;
import com.project.gobarber.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> optionalUser = userRepository.findByName(username);
        optionalUser
                .orElseThrow(() -> new UsernameNotFoundException("User name not found"));
        return optionalUser
                .map(UserDetailsImpl::new).get();
    }
}
