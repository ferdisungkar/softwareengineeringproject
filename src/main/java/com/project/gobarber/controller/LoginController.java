package com.project.gobarber.controller;

import com.project.gobarber.model.Role;
import com.project.gobarber.model.User;
import com.project.gobarber.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/")
public class LoginController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/login")
    public String getLoginPage() {
        return "login";
    }

    @PostMapping("/login")
    public String postLogin(@RequestBody MultiValueMap<String, String> formData) {
        String email = formData.getFirst("email");
        String password = formData.getFirst("password");
        User user = this.userRepository.getUserByEmail(email);
        if (user.getPassword().equals(password)) {
//                Cookie cookie = new Cookie("user_email", user.getEmail());
//                cookie.setMaxAge(3600 * 24);
//                cookie.setHttpOnly(true);
//                cookie.setPath("/");
//                response.addCookie(cookie);
            return "redirect:/home/";
        }

        return "redirect:/";
    }

    @GetMapping("/logout")
    public String postLogout() {
//        Cookie cookie = new Cookie("user_id", null);
//        cookie.setMaxAge(0);
//        cookie.setHttpOnly(true);
//        cookie.setPath("/");
//        response.addCookie(cookie);
        return "redirect:/";
    }

    @GetMapping("/signup")
    public String getSignupPage () {
//        if (!userEmail.isEmpty()) return "redirect:/home/";
        return "signup";
    }

    @PostMapping("/signup")
    public String postSignup(@RequestBody MultiValueMap<String, String> formData) {
//        if (!userEmail.isEmpty()) return "redirect:/home/";
        // TODO: form validation
        String name = formData.getFirst("name");
        String email = formData.getFirst("email");
        String password = formData.getFirst("password");
        Role role = Role.ROLE_ADMIN;

        User user = new User(email, password, name, role);
        this.userRepository.save(user);
        return "redirect:/login";
    }


}
