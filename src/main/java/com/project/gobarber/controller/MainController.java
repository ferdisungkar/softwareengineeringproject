package com.project.gobarber.controller;

import com.project.gobarber.model.Booking;
import com.project.gobarber.model.User;
import com.project.gobarber.repository.BookingRepository;
import com.project.gobarber.repository.UserRepository;
import com.project.gobarber.servive.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Controller
@RequestMapping("/home/")
public class MainController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    BookingRepository bookingRepository;

    @GetMapping("/")
    public String getHomePage() { return "home"; }

    @GetMapping("/booking")
    public String getBookingPage() { return "booking";}

    @PostMapping("/booking")
    public String bookingPost(@RequestBody String email, String bookingPlace,
                              String bookingDate, String bookingTime) {
        User user = this.userRepository.getUserByEmail(email);
        LocalDate localDate = LocalDate.parse(bookingDate);
        LocalTime localTime = LocalTime.parse(bookingTime);
        Booking booking = new Booking(user, bookingPlace, localDate, localTime);
        bookingRepository.save(booking);

        return "redirect:/home/";
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    @RequestMapping(value = "/admin/booking")
    public String getBookingAdmin(Model model) {
        List<Booking> bookings = bookingRepository.findAll();
        model.addAttribute("bookings", bookings);
        return "bookingViewAdmin";
    }
}
