package com.project.gobarber.model;

import com.sun.istack.NotNull;

import javax.persistence.*;

@Table(name = "rewards")
@Entity
public class Reward {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "reward_id")
    private Long id;

    @NotNull
    @Column(name = "reward_name")
    private String rewardName;

    @NotNull
    @Column(name = "minimal_bookings")
    private int minimalBookingsMade;

    public Reward(String rewardName, int minimalBookingsMade) {
        this.rewardName = rewardName;
        this.minimalBookingsMade = minimalBookingsMade;
    }

    public Reward() {
    }

    public String getRewardName() {
        return rewardName;
    }

    public void setRewardName(String rewardName) {
        this.rewardName = rewardName;
    }

    public int getMinimalBookingsMade() {
        return minimalBookingsMade;
    }

    public void setMinimalBookingsMade(int minimalBookingsMade) {
        this.minimalBookingsMade = minimalBookingsMade;
    }
}
