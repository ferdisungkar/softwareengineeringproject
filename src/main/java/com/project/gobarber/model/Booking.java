package com.project.gobarber.model;

import com.sun.istack.NotNull;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Table(name = "bookings")
@Entity
public class Booking {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "booking_id")
    private Long id;

    @ManyToOne
    private User booker;

    @NotNull @Column(name = "booking_place")
    private String bookingPlace;

    @NotNull @Column(name = "booking_date")
    private LocalDate bookingDate;

    @NotNull @Column(name = "booking_time")
    private LocalTime bookingTime;

//    @ManyToOne
//    private Schedule bookingSchedule;

    public Booking(User booker, String bookingPlace, LocalDate bookingDate, LocalTime bookingTime) {
        this.booker = booker;
        this.bookingPlace = bookingPlace;
        this.bookingDate = bookingDate;
        this.bookingTime = bookingTime;
    }

    public Booking() {
    }

    public String getBookingPlace() {
        return bookingPlace;
    }

    public void setBookingPlace(String bookingPlace) {
        this.bookingPlace = bookingPlace;
    }

    public User getBooker() { return booker; }

    public void setBooker(User booker) { this.booker = booker; }

    public LocalDate getBookingDate() { return bookingDate; }

    public void setBookingDate(LocalDate bookingDate) { this.bookingDate = bookingDate; }

    public LocalTime getBookingTime() { return bookingTime; }

    public void setBookingTime(LocalTime bookingTime) { this.bookingTime = bookingTime; }
}
